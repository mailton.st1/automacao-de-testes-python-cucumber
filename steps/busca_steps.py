from behave import given, when, then

# importar class de utilidades (browser)
from utils import Utils

#importar metodos de paginas
from pages.google_home_page import GoogleHomePage
from pages.google_results_page import GoogleResultsPage
from pages.google_results_pesquisa_page import GoogleResultsPesquisaPage

from time import sleep

#importar biblioteca de asserção =  (nose)
from nose.tools import *

utils = Utils()
google_home_page = GoogleHomePage()
google_results_page = GoogleResultsPage()
google_results_pesquisa = GoogleResultsPesquisaPage()

class ResultadoEsperado(object):
    TEXTO_PARAGRAFO = 'Acesso, segurança e diversificação para você e mais de 2 milhões de clientes.'

@given(u'que acesso o google')
def step_impl(context):
    utils.navegar('https://www.google.com.br/')
 
@given(u'faço uma pesquisa especifica acessando o botão pesquisar')
def step_impl(context):
    google_home_page.preenche_input_busca('mercado bitcoin')
    google_home_page.click_btn_go()


@when(u'acesso o site que veio  de reultado no google')
def step_impl(context):
    google_results_pesquisa.clicar_link_site_pesquisa()


@then(u'devo visualizar a pagina de home do site que acessei')
def step_impl(context):
    assert_equal(google_results_page.get_text_paragrafo_site(), ResultadoEsperado.TEXTO_PARAGRAFO)
    utils.closeBrowser()
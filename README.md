**# automação-de-testes-python-cucumber**

**###Configuração do Ambiente**

 - Instale a versão atualizada do Python**
        

- Instale as dependências necessárias para o projeto de automação.
    
    **Instale as dependencias executando no terminal os seguintes comandos:**
        
        ```
            pip install selenium
            pip install behave
            pip install nose
        ```

    


**###Utilizando o Behave**
- Behave é um framework que permite a escrita de testes automatizados em BDD (Behaviour-Driven Development) no Python.           Utilizaremos ele juntamente com o Selenium para estruturar nosso projeto.

**####Estrutura do projeto**


1. **####Features:** 
- Pasta que armazena os arquivos .feature utilizados para a escrita dos testes em BDD.


2. **####Pages:**
- Armazena os arquivos que contêm os seletores e os comandos executados em cada página.


3. **####Steps:** 
- Armazena os arquivos com os steps do BDD. Responsável por conectar os steps escritos em BDD com o código das pages.


4. **####Browser.py:** 
- Arquivo de configurações relacionadas ao browser.


5. **####Environment.py:** 
- Arquivo de configurações relacionadas à execução dos testes.


6. **##Execução do teste:**
- Finalizada a implementação dos cenários em BDD nos arquivos features, basta executar os comandos no terminal, na raiz do projeto para gerar os **Steps** dos cenários de testes.- 
    
  
* Execute o comando `"behave"` no terminal raiz do projeo (Irá gerar os Steps no terminal, então poderá copiar os steps para criar os arquivos_steps.py).
      
* Para executar algum cenário especifico execute o comando `"behave --tags=@tags"` no terminal na raiz do projeo (Irá executar apenas os cenários que estiverem com as @tags selecionadas).





###### Autor: 
```Mailton Nascimento```



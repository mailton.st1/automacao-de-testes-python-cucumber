from browser import Browser

class GooglePageElements(object):
    INPUT_PESQUISAR = 'input[type="text"]'
    BTN_PESQUISA_GO = '#tsf > div:nth-child(2) > div.A8SBwf > div.FPdoLc.tfB0Bf > center > input.gNO89b'

class GoogleHomePage(Browser):

    def preenche_input_busca(self, texto):
        self.driver.find_element_by_css_selector(GooglePageElements.INPUT_PESQUISAR).send_keys(texto)


    def click_btn_go(self):
        self.driver.find_element_by_css_selector(GooglePageElements.BTN_PESQUISA_GO).click()  
    

    
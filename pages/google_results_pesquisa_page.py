from browser import Browser

class GoogleResultsPesquisaPageElements(object):
    LINK_MERCADO_BITCOIN = 'a[href="https://www.mercadobitcoin.com.br/"]'

class GoogleResultsPesquisaPage(Browser):

    def clicar_link_site_pesquisa(self):
        self.driver.find_element_by_css_selector(GoogleResultsPesquisaPageElements.LINK_MERCADO_BITCOIN).click()

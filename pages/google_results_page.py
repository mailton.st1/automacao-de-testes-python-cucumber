from browser import Browser

class GoogleResultsPageElements(object):
    RESULTS_TEXT_P = 'body > section.hero > div > div.text-content > p:nth-child(2)'

class GoogleResultsPage(Browser):

    def get_text_paragrafo_site(self):
        return self.driver.find_element_by_css_selector(GoogleResultsPageElements.RESULTS_TEXT_P).text

#language: pt

Funcionalidade: Fluxo de busca

@pesquisar
Cenário: realizar pesquisa no google
    Dado que acesso o google
    E faço uma pesquisa especifica acessando o botão pesquisar
    Quando acesso o site que veio  de reultado no google
    Então devo visualizar a pagina de home do site que acessei
